angular.module("cn", ["ngMaterial", "monospaced.qrcode"])
	.controller("cnGame", function($scope, $http, $window, $mdToast, $mdDialog, $mdSidenav, $location, CodeNames) {
		$scope.game = {};
		$scope.data = {
			language: "en-US",
			player: {},
			words: [],
			gameMode: "2p",
		};
		$scope.languages = {
			"en-US": "English", 
			"pt-BR": "Português (Brasil)"
		};
		$scope.CodeNames = CodeNames;
		$scope.$mdDialog = $mdDialog;
		
		var searchParams = $location.search();
		$scope.data.debug = searchParams.debug;
		var PlayerID = function(id) {
			$scope.data.player.playerID = id;
			CodeNames.player = id;
			
			$location.hash(id);
			/*
			angular.forEach(searchParams, function(value, key) {
				$location.search(key, undefined);
			});
			*/
			$scope.data.url = $location.absUrl();
		}
				
		$scope.GameCreate = function() {
			// Generate the game data
			$scope.game = CodeNames.GameNew();
			CodeNames.GameMode($scope.data.gameMode);
			CodeNames.gameData.allowUnlimitedTips = $scope.data.allowUnlimitedTips;
			CodeNames.gameData.tipMinimun = $scope.data.allowZeroTips ? 0 : 1;
			CodeNames.gameData.AIMaster = $scope.data.player.playerID;
			
			CodeNames.PlayerAdd({playerID: $scope.data.player.playerID, team: 1, master: true, name: $scope.data.player.name});
			$scope.data.player = CodeNames.gameData.players[$scope.data.player.playerID];
			
			// Generate the words
			$scope.LoadWords(function() {
				$scope.GameNewMatch();
			});
		}
		
		$scope.GameClear = function() {
			CodeNames.GameClear();
		}
		
		$scope.GameJoin = function() {
			console.debug("CodeNames: " + "Trying to connect to: " + $scope.data.peerID);
			var conn = peer.connect($scope.data.peerID);
			conn.on("open", function() {
				PeerConnectToOthersFull(conn);
			});
			conn.on("error", function(error){
				console.debug("CodeNames: " + error);
			});
		}
		
		$scope.GameStart = function() {
			CodeNames.GameStart();
			PeerSendDataToALL("GameStart");
		}
		
		$scope.GameCanStart = function() {
			return $scope.PlayerExistAll();
		}
		
		$scope.GameNewMatch = function() {
			CodeNames.GameNewMatch(CodeNames.CardsCreate($scope.data.words));
			PeerSendDataToALL("GameNewMatch", CodeNames.gameData.cards);
		}
		
		$scope.GameRefresh = function () {
			$location.hash("");
			$window.location.href = $location.absUrl();
			$window.location.reload();
		}
		
		$scope.PlayerChange = function(player) {
			PeerSendDataToALL("PlayerChange", player);
			CodeNames.PlayerChange(player);
		}
		
		$scope.PlayerChangeSwapTeam = function(player) {
			if (player === undefined)
				player = $scope.data.player;
			player.team = player.team == 2 ? 1 : 2;
			$scope.PlayerChange(player);
		}
		
		$scope.PlayerChangeSwapRole = function(player) {
			if (player === undefined)
				player = $scope.data.player;
			player.master = !player.master;
			$scope.PlayerChange(player);
		}
		
		$scope.PlayerReady = function() {
			CodeNames.PlayerReady($scope.data.player);
			PeerSendDataToALL("PlayerReady", $scope.data.player);
		}
		
		$scope.PlayerExistAll = function() {
			return $scope.PlayerTypeExist(true, 1) && $scope.PlayerTypeExist(true, 2)
			//$scope.PlayerTypeExist(false, 1) && $scope.PlayerTypeExist(false, 2);
		}
		
		$scope.PlayerTypeExist = function(master, team) {
			return CodeNames.PlayerTypeExist(master, team);
		}
		
		$scope.LoadWords = function(callback) {
			var txtFile = new XMLHttpRequest();
			txtFile.open("GET", "words/" + $scope.data.language + ".txt", true);
			txtFile.onreadystatechange = function()
			{
				if (txtFile.readyState === 4) {  // document is ready to parse.
					if (txtFile.status === 200) {  // file is found
						allText = txtFile.responseText; 
						$scope.data.words = txtFile.responseText.split("\n");
						
						if (callback !== undefined) {
							callback();
						}
					}
				}
			}
			txtFile.send(null);
		}
		
		$scope.DecisionTip = function(data) {
			PeerSendDataToALL("DecisionTip", data);
			CodeNames.DecisionTip(data);
		}
		
		$scope.DecisionPass = function(data) {
			PeerSendDataToALL("DecisionPass");
			CodeNames.DecisionPass();
		}
		
		$scope.DecisionCardSelect = function(card, force) {
			if (CodeNames.DecisionCardSelectCheck(card) && (($scope.data.player.master == false && $scope.data.player.team == CodeNames.gameData.team) || force)) {
				PeerSendDataToALL("DecisionCardSelected", card);
				CodeNames.DecisionCardSelect(card);
			}
		}
		
		$scope.CanPass = function() {
			CodeNames.CanPass();
		}
		
		$scope.GameOverDialog = function(ev) {
			//var teamWin = (CodeNames.gameData.qtyCards[1] - CodeNames.gameData.score[1] == 0) ? 1 : 2;
			
			//var win = $scope.data.player.team == teamWin;
			
			$mdDialog
				.show( {
					templateUrl: "gameover.html",
					parent: angular.element(document.body),
					//clickOutsideToClose:true,
					//escapeToClose: true,
					fullscreen: true,
					controller: function($scope, $mdDialog) {
						$scope.answer = function(answer) {
							$mdDialog.hide(answer);
						};
					}
				} )
				.then(function(answer) {
					console.debug(answer);
					if (answer == "rematch")
						$scope.GameNewMatch();
				}, function() {
					$scope.GameClear();
					console.debug('You cancelled the dialog.');
				});
		}
		
		var connections = {};
	
		var PeerConnect = function (conn){
			connections[conn.peer] = conn;
			
			conn.on("data", PeerDataReceived);

			conn.on("close", function(){
				delete connections[conn.peer];
				delete CodeNames.gameData.players[conn.peer];
				console.error(conn.peer + " has left the game");
				$scope.$apply();
			});
		}
		
		var PeerConnectToOthersFull = function(conn) {
			PeerConnectToOthers(conn);
			
			// Ask for the game data
			conn.send({peerID: $scope.data.player.playerID, op: "GameRequest", data: ""});
		}
		
		var PeerConnectToOthers = function(conn) {
			PeerConnect(conn);
			console.debug("CodeNames: " + "Connected to player: " + conn.peer);
		}
		
		var PeerConnectedFromOther = function(conn) {
			PeerConnect(conn);
			console.debug("CodeNames: " + "New player: " + conn.peer);
			
			// Register the new player
			CodeNames.PlayerAdd(CodeNames.PlayerCreate(conn.peer));
			$scope.$apply();
		}

		var PeerDataReceived = function(data) {
			if (data.op == "game") {
				$scope.game = CodeNames.GameSetup(data.data);
				
				CodeNames.gameData.players[$scope.data.player.playerID].name = $scope.data.player.name;
				$scope.data.player = CodeNames.gameData.players[$scope.data.player.playerID];
				
				angular.forEach(CodeNames.gameData.players, function(player) {
					if (player.playerID != $scope.data.player.playerID && connections[player.playerID] === undefined) {
						var conn = peer.connect(player.playerID);
						conn.on("open", function() {
							PeerConnectToOthers(conn);
						});
						conn.on("error", function(error){
							console.debug("CodeNames: " + error);
						});
					}
				});
				
				$scope.$apply();
				
				$scope.PlayerChange($scope.data.player);
			}
			else if (data.op == "GameNewMatch") {
				CodeNames.GameNewMatch(data.data);
				$scope.$apply();
			}
			else if (data.op == "GameRequest") {
				connections[data.peerID].send({peerID: $scope.data.player.playerID, op: "game", data: CodeNames.gameData});
			}
			else if (data.op == "GameStart") {
				CodeNames.GameStart();
				$scope.$apply();
			}
			else if (data.op == "DecisionTip") {
				CodeNames.DecisionTip(data.data);
				$scope.$apply();
			}
			else if (data.op == "DecisionPass") {
				CodeNames.DecisionPass();
				$scope.$apply();
			}
			else if (data.op == "DecisionCardSelected") {
				var DecisionCardSelected;
				angular.forEach(CodeNames.gameData.cards, function(card) {
					if (data.data.word == card.word) {
						DecisionCardSelected = card;
					}
				})
				CodeNames.DecisionCardSelect(DecisionCardSelected);
				$scope.$apply();
			}
			else if (data.op == "PlayerChange") {
				CodeNames.PlayerChange(data.data);
				$scope.$apply();
			}
			else if (data.op == "PlayerReady") {
				CodeNames.PlayerReady(data.data);
				$scope.$apply();
			}
		}
		
		var PeerSendDataToALL = function(op, data) {
			angular.forEach(connections, function(conn, perr) {
				conn.send({peerID: $scope.data.player.playerID, op: op, data: data});
			});
		}
		
		var ping = function() {
			angular.forEach(connections, function(conn, perr) {
				conn.socket.send({type: 'ping'});
			});
			timeoutID = setTimeout(ping, 20000);
		}
		
		//Get the ID from the server
		// For HTTPS connections, use the PeerJS implmented in my own server.
		// For local 
		if (searchParams.local)
			var peer = new Peer({key: 'n8l4sqfxvh8y3nmi'});
		else
			var peer = new Peer({host: "games.brunomassa.com", port: "", path: "peerjs"});

		//to receive id from the server
		peer.on("open", function(id){
			console.log("Your playerID is: "  +id);
			PlayerID(id);
			$scope.$apply();
		});

		//in case of error
		peer.on("error", function(e){
			console.debug("CodeNames: " + e.message);
			//alert(e.message);
		})

		//Awaits for the connection
		peer.on("connection", PeerConnectedFromOther);

		// Try to load automatically
		var peerID = $location.hash();
		if (peerID !== undefined && peerID !== "" && peerID !== $scope.data.player.playerID) {
			$scope.data.peerID = peerID;
			$scope.GameJoin();
		}
		
		// Define the language. Try first to get the language from the browser first
		var language = window.navigator.userLanguage || window.navigator.language;
		if ($scope.languages[language] !== undefined)
			$scope.data.language = language;
		
		ping();
		$scope.$watch("CodeNames.gameData.stage", function(newValue, oldValue) {
			if (newValue == "gameOver") {
				$scope.GameOverDialog();
			}
			else if (newValue == "tip") {
				if (CodeNames.gameData.AIMaster == $scope.data.player.playerID && CodeNames.gameData.team == 2 && (CodeNames.gameData.gameMode == "2p" || CodeNames.gameData.gameMode == "3p1master"))
					//CodeNames.AIMaster();
					$scope.DecisionTip({
						tip: "",
						number: 1
					});
			}
			else if (newValue == "guess") {
				if (CodeNames.gameData.AIMaster == $scope.data.player.playerID && CodeNames.gameData.team == 2 && (CodeNames.gameData.gameMode == "2p" || CodeNames.gameData.gameMode == "3p1master"))
					//CodeNames.AIGuesser();
					$scope.DecisionCardSelect(CodeNames.AIGuesserCard(), true);
			}
		});
	})
	.service("CodeNames", function($http) {
		var CodeNames = {};
		
		CodeNames.gameData = {};
		
		CodeNames.GameNew = function() {
			CodeNames.gameData = {
				team: 2,
				turn: 0,
				tipMinimun: 1,
				stage: "waitPlayers",
				qtyCards: [
					7, // team 0 - neutral
					9, // team 1 - team 1
					8, // team 2 - team 2
					1, // team 3 - spy
				],
				score: [0, 0, 0, 0],
				players: {},
				cards: [],
				gameMode: "4p"
			};
			return CodeNames.gameData;
		}
		
		CodeNames.GameClear = function() {
			CodeNames.gameData.stage = "";
		}
		
		CodeNames.GameNewMatch = function(cards) {
			angular.forEach(CodeNames.gameData.players, function(player) {
				player.ready = false;
			});
			
			CodeNames.gameData.team = 2;
			CodeNames.gameData.turn = 0;
			CodeNames.gameData.score = [0, 0, 0, 0];
			CodeNames.gameData.cards = cards;
			CodeNames.gameData.stage = "waitPlayers";
		}
		
		CodeNames.GameSetup = function(game) {
			CodeNames.gameData = game;
			CodeNames.gameData.score = CodeNames.ScoreCheck();
			return CodeNames.gameData;
		}
		
		CodeNames.GameStart = function() {
			CodeNames.TeamNext();
		}
		
		CodeNames.GameStartCheck = function() {
			var minPlayers;
			if (CodeNames.gameData.gameMode == "2p")
				minPlayers = CodeNames.PlayerTypeExist(true, 1) && CodeNames.PlayerTypeExist(false, 1);
			else if (CodeNames.gameData.gameMode == "3p1master")
				minPlayers = CodeNames.PlayerTypeExist(true, 1) && CodeNames.PlayerTypeExist(false, 1) && CodeNames.PlayerTypeExist(false, 2);
			else if (CodeNames.gameData.gameMode == "3p2master")
				minPlayers = CodeNames.PlayerTypeExist(true, 1) && CodeNames.PlayerTypeExist(true, 2) && CodeNames.PlayerTypeExist(false, 1);
			else
				minPlayers = CodeNames.PlayerTypeExist(true, 1) && CodeNames.PlayerTypeExist(false, 1) && CodeNames.PlayerTypeExist(true, 2) && CodeNames.PlayerTypeExist(false, 2);
			
			var everyoneReady = true;
			angular.forEach(CodeNames.gameData.players, function(player) {
				everyoneReady = everyoneReady && player.ready;
			});
			
			return minPlayers && everyoneReady;
		}
		
		CodeNames.GameMode = function(gameMode) {
			CodeNames.gameData.gameMode = gameMode;
		}
		
		CodeNames.GameOverCheck = function(card) {
			if (card !== undefined) {
				if (card.team == 3)
					return true;
			}
			
			var score = [0, 0, 0, 0];
			angular.forEach(CodeNames.gameData.cards, function(card) {
				if (card.revealed) {
					score[card.team]++;
				}
			});
			CodeNames.gameData.score = score;
			
			return (CodeNames.gameData.score[1] == CodeNames.gameData.qtyCards[1] || CodeNames.gameData.score[2] == CodeNames.gameData.qtyCards[2]);
		}
		
		CodeNames.PlayerCreate = function(playerID) {
			return {
				playerID: playerID,
				team: ( (CodeNames.gameData.gameMode == "2p" || CodeNames.gameData.gameMode == "3p1master") ? 1 : 2),
				master: false,
				name: ""
			};
		}
		
		CodeNames.PlayerAdd = function(player) {
			if (CodeNames.gameData.players[player.playerID] === undefined) {
				CodeNames.gameData.players[player.playerID] = player;
			}
		}
		
		CodeNames.PlayerChange = function(player) {
			if (CodeNames.gameData.players[player.playerID] !== undefined) {
				CodeNames.gameData.players[player.playerID] = player;
			}
		}
		
		CodeNames.PlayerReady = function(player) {
			CodeNames.gameData.players[player.playerID].ready = true;
			
			if (CodeNames.GameStartCheck())
				CodeNames.GameStart();
		}
		
		CodeNames.PlayerTypeExist = function(master, team) {
			var masterExist = false;
			angular.forEach(CodeNames.gameData.players, function(player) {
				if (player.master == master && player.team == team) {
					masterExist = true;
					return;
				}
			});
			return masterExist;
		}
		
		CodeNames.ScoreCheck = function() {
			var score = {1: 0, 2: 0};
			
			angular.forEach(CodeNames.gameData.cards, function(card) {
				if (card.revealed == true) {
					if (card.team == 1 || card.team == 2)
						score[card.team] = score[card.team] + 1;
				}
			});
			
			return score;
		}
		
		CodeNames.DecisionTip = function(tipData) {
			CodeNames.gameData.tip = {
				tip: tipData.tip,
				number: tipData.number,
				remain: tipData.number,
				unlimited: tipData.unlimited
			}
			CodeNames.gameData.stage = "guess";
		}
		
		CodeNames.DecisionPass = function() {
			CodeNames.TeamNext();
		}
		
		CodeNames.DecisionCardSelect = function(card) {
			if (CodeNames.DecisionCardSelectCheck(card)) {
				card.revealed = true;
				
				if (card.team != CodeNames.gameData.team)
					CodeNames.gameData.tip.remain = 0;
				else
					CodeNames.gameData.tip.remain--;
				
				if (CodeNames.GameOverCheck(card)) {
					CodeNames.gameData.stage = "gameOver";
				} else if (card.team != CodeNames.gameData.team) {
					CodeNames.TeamNext();
				} else if (!CodeNames.gameData.tip.unlimited && CodeNames.gameData.tip.remain <= 0) {
					CodeNames.TeamNext();
				}
			}
		}
		
		CodeNames.DecisionCardSelectCheck = function(card) {
			return (CodeNames.gameData.stage == "guess" && !card.revealed && (CodeNames.gameData.tip.remain > 0 || CodeNames.gameData.tip.unlimited))
		}
		
		CodeNames.CardsCreate = function(words) {
			CodeNames.Shuffle(words);
			var cards = [];
			var team = 0;
			var qty = CodeNames.gameData.qtyCards[team];
			for (var i = words.length - 1; i > 0; i--) {
				if (qty <= 0) {
					if (team++ >= CodeNames.gameData.qtyCards.length)
						break;
					qty = CodeNames.gameData.qtyCards[team];
					if (qty === undefined)
						break;
				}
				var card = {
					word: "", 
					team: team, 
					//revealed: qty > 1,
					revealed: false
				};
				card.word = words[i];
				cards.push(card);
				qty--;
			}
			
			CodeNames.Shuffle(cards);
			return cards;
		}
		
		CodeNames.TeamOther = function(team, force) {
			if (team === undefined && force === undefined)
				return CodeNames.TeamOther(CodeNames.gameData.team, true );
			
			return (CodeNames.gameData.team == 1) ? 2 : 1;
		}
		
		CodeNames.TeamNext = function() {
			if (CodeNames.gameData.team == 2)
				CodeNames.gameData.turn++;
			CodeNames.gameData.team = CodeNames.TeamOther();
			
			CodeNames.gameData.tip = {number: 1};
			CodeNames.gameData.stage = "tip";
		}
		
		CodeNames.Shuffle = function(array) {
			var currentIndex = array.length, temporaryValue, randomIndex ;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {
				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		}
				
		CodeNames.CanPass = function () {
			return CodeNames.gameData.tip !== undefined && CodeNames.gameData.tip.remain != CodeNames.gameData.tip.number;
		}
				
		CodeNames.AIGuesserCard = function() {
			var cardsTeam2 = [];
			angular.forEach(CodeNames.gameData.cards, function(card) {
				if (card.team == 2 && !card.revealed)
					cardsTeam2.push(card);
			});
			return CodeNames.Shuffle(cardsTeam2)[0];
		}
		
		return CodeNames;
	})
	.directive("cnTeamBadge", function() {
		return {
			restrict: "E",
			scope: {
			  team: "=",
			},
			transclude: true,
			template: "<div class='revealed teamBadge' ng-class='{neutral: team == 0, team1: team == 1, team2: team == 2, spy: team == 3}'><ng-transclude layout='column' layout-align='center center' flex></ng-transclude></div>",
			//template: "<div class='revealed teamBadge' ng-class='{neutral: team == 0, team1: team == 1, team2: team == 2, spy: team == 3}'></div>",
		};
	})
	.directive("cnCard", function() {
		return {
			restrict: "E",
			scope: {
			  card: "=",
			  player: "=",
			  callback: "&"
			},
			templateUrl: "card.html",
			link: function($scope) {
				$scope.CardClicked = function(card) {
					$scope.callback({param1: card});
				}
			}
		};
	})
	.directive("cnTip", function() {
		return {
			restrict: "E",
			scope: {
			  callback: "&",
			  game: "="
			},
			templateUrl: "tip.html",
			link: function($scope) {
				$scope.tipData = {};
				
				$scope.SendTip = function() {
					$scope.callback({param1: $scope.tipData});
					$scope.tipData = {};
				}
			}
		};
	})
	.directive("cnPlayer", function() {
		return {
			restrict: "E",
			scope: {
			  player: "="
			},
			templateUrl: "player.html",
		};
	})
	.directive("cnLegend", function() {
		return {
			restrict: "E",
			templateUrl: "legend.html",
		};
	})
	.filter("pager", function() {
		return function(cards, row) {
			var cardsInRow = [];
			angular.forEach(cards, function(card, cardNum) {
				if (Math.floor(cardNum / 5) == row)
					cardsInRow.push(card);
			});
			return cardsInRow;
		};
	})
	.filter('toArray', function () {
	   return function (obj, addKey) {
		if (!angular.isObject(obj)) return obj;
		 if ( addKey === false ) {
		   return Object.keys(obj).map(function(key) {
			 return obj[key];
		   });
		 } else {
		   return Object.keys(obj).map(function (key) {
			var value = obj[key];
			return angular.isObject(value) ?
			  Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
			  { $key: key, $value: value };
		   });
		 }
	   };
	 })
	.config(function($mdThemingProvider) {
		$mdThemingProvider.theme("default")
			.primaryPalette("amber")
			.accentPalette("blue")
			.warnPalette("red")
			.backgroundPalette("blue-grey")
			;
	})
;